#include "parsers.h"
#include "ocv.h"
#include "libVDA/VDA.h"
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
#if 1
    Options opts;
    int cmd; /* -3 -- err; -2 -- none; -1 -- help; 0 -- teach; 1 -- synth */
    CvMat *imgd, *img1, *img2, *img3;
    char const *dfname;

    if (argc < 2) {
        printf("error: command or help option is missed\n");
        return (-1);
    }
    if (-1 == (cmd = parseopt(opts_init(&opts), argc, argv))) {
        return (0);
    }
    if (-2 == cmd) {
        printf("error: command or help option is missed\n");
        return (-1);
    }
    if (-3 == cmd) {
        printf("error: error while parsing options\n");
        return (-2);
    }
    if (opts_default(&opts)) {
        printf("error: one of main options is missed\n");
        return (-3);
    }

    dfname = (1 == cmd ? opts.opts_src1 : opts.opts_dst);
    if (0 != ocv_readImage(&imgd, dfname, 1)
        || 0 != ocv_readImage(&img1, opts.opts_src1, 0)
        || 0 != ocv_readImage(&img2, opts.opts_src2, 0)
        || 0 != ocv_readImage(&img3, opts.opts_src3, 0)) {
        printf("Image file does not exists\n");
        return (-2);
    }

    if ((0 == cmd) && (0 != VDATeachByImage(imgd, img1, img2, img3, opts.opts_wlen1, opts.opts_wlen2, opts.opts_wlen3/*850, 940, 870*/))) {
        printf("Error while processing\n");
        return (-4);
    }
    if ((1 == cmd) && (0 != VDASynthesis(imgd, img1, img2, img3, opts.opts_wlen1, opts.opts_wlen2, opts.opts_wlen3/*850, 940, 870*/))) {
        printf("Error while processing\n");
        return (-4);
    }

    ocv_readImage(&imgd, "/home/admin-1/Pictures/res3.bmp", 1);
    if ((1 == cmd) && (0 != ocv_writeImage(imgd, opts.opts_dst))) {
        printf("Error while saving image\n");
        cvReleaseMat(&imgd);
        cvReleaseMat(&img1);
        cvReleaseMat(&img2);
        cvReleaseMat(&img3);
        return (-3);
    }

    cvReleaseMat(&imgd);
    cvReleaseMat(&img1);
    cvReleaseMat(&img2);
    cvReleaseMat(&img3);
#elif 1 /* ir: 840,420 1640,930 rgb: 794,426 1576,941 */
     /* ir: 862,440 1612,933 rgb: 816,446 1548,944 */
    CvMat *img;
    int cols, rows;
    ocv_readImage(&img, argv[1], 0);
    img->data.ptr += img->cols * 420 + 840;
    cols = img->cols;
    rows = img->rows;
    img->cols = 800;
    img->rows = 510;
    ocv_writeImage(img, argv[1]);
    img->data.ptr -= cols * 420 + 840;
    img->cols = cols;
    img->rows = rows;

    cvReleaseMat(&img);
#else
    CvMat *img, *dst;
    ocv_readImage(&img, argv[1], 1);
    dst = cvCreateMat(510, 800, CV_8UC3);
    cvResize(img, dst, CV_INTER_CUBIC);
    ocv_writeImage(dst, argv[1]);

    cvReleaseMat(&img);
    cvReleaseMat(&dst);
#endif

    return (0);
}

all:
	gcc -x c++ -c -Wall -Werror -fpic libVDA/widths.c libVDA/neural.c libVDA/synthesis.c
	gcc -shared -o libVDA/libVDA.so widths.o neural.o synthesis.o
	gcc -L/home/admin-1/prog/diploma/libVDA/ -Wall -o b.out -x c++ ocv.c main.c options.c parseopt.c parsers.c -lopencv_imgcodecs -lopencv_core -lVDA

lib:
	gcc -x c++ -c -Wall -Werror -fpic libVDA/widths.c libVDA/neural.c libVDA/synthesis.c
	gcc -shared -o libVDA/libVDA.so widths.o neural.o synthesis.o

testprog:
	gcc -L/home/admin-1/prog/diploma/libVDA/ -Wall -o b.out -x c++ ocv.c main.c options.c parseopt.c parsers.c -lopencv_imgcodecs -lopencv_core -lVDA

clean:
	rm *.o

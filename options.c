#include "parsers.h"

static int _t_inP(Options *anOpts, char *aStr)
{
    char *x, *y, *z;
    if (NULL == aStr) {
        return 0;
    }
    if ('(' != aStr[0]) {
        return 0;
    }
    x = aStr + 1;
    while ('\0' != *aStr && ',' != *aStr) {
        aStr++;
    }
    if ('\0' == *aStr) {
        return 0;
    }
    y = ++aStr;
    while ('\0' != *aStr && ',' != *aStr) {
        aStr++;
    }
    if ('\0' == *aStr) {
        return 0;
    }
    z = ++aStr;
    while ('\0' != *aStr && ')' != *aStr) {
        aStr++;
    }
    if ('\0' == *aStr) {
        return 0;
    }
    if ('\0' == *(aStr + 1)) {
        anOpts->opts_src1 = x;
        anOpts->opts_src2 = y;
        *(y - 1) = '\0';
        anOpts->opts_src3 = z;
        *(z - 1) = '\0';
        *aStr = '\0';
        return (1);
    }
    return (0);
}

static int _t_outP(Options *anOpts, char *aStr)
{
    anOpts->opts_dst = aStr;
    return ('\0' != *aStr);
}

static int _t_lenP(Options *anOpts, char *aStr)
{
    if (!int3P(&anOpts->opts_wlen1, &anOpts->opts_wlen2, &anOpts->opts_wlen3, aStr, (char const **)&aStr) && eofP(aStr)) {
        return (0);
    }
    return (1);
}

static int _t_objP(Options *anOpts, char *aStr)
{
    anOpts->opts_objmap = aStr;
    return ('\0' != *aStr);
}

static int _t_widP(Options *anOpts, char *aStr)
{
    anOpts->opts_widths = aStr;
    return ('\0' != *aStr);
}

Option const _G_global_options[] = {
    { "i", "input", _t_inP, "R(FilePath, FilePath, FilePath)", "file paths for IR input images" },
    { "o", "output", _t_outP, "RFilePath", "output/etalon 3-nchannel image" },
    { "l", "wave-len", _t_lenP, "R(int, int, int)", "lengths of input waves" },
    { "m", "obj-map", _t_objP, "RFilePath", "object map" },
    { "w", "widths", _t_widP, "RFilePath", "input/output widths" },

    { NULL }
};

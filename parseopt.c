#include "parsers.h"

static Option const *
_t_short_lookup(char aName, Option const *anOptions)
{
	Option const *opt;

	for (opt = anOptions; opt->opt_short; ++opt) {
		if (*(opt->opt_short) == aName) {
			return (opt);
		}
	}
	return (NULL);
}

static Option const *
_t_long_lookup(char const *aName, Option const *anOptions)
{
    Option const *opt;
    size_t i;
    for (opt = anOptions; opt->opt_short; ++opt) {
        for (i = 0; aName[i] && aName[i] == opt->opt_long[i]; ++i);
        if ('\0' == aName[i] && ('\0' == opt->opt_long[i] || isspace(opt->opt_long[i]))) {
            return (opt);
        }
    }
    return (NULL);
}

static int _t_print_spaces(int aCount)
{
	int n;

	for (n = 0; n < aCount; ++n)
		putchar(' ');
	return (n);
}

static void _t_print_options(Option const *anOpts)
{
	Option const *opt;
	int n;

	for (opt = anOpts; NULL != opt->opt_short; ++opt) {
		n = printf("  ");
		n += printf("-%s", opt->opt_short);

		n += _t_print_spaces(5 - n);
		n += _t_print_spaces(1);
		n += printf("--%s ", opt->opt_long);

		n += _t_print_spaces(22 - n);
		switch (opt->opt_argtyp[0]) {
		case 'N':
			break;
		case 'R':
			n += printf(" %s", opt->opt_argtyp + 1);
			break;
		case 'O':
			n += printf(" [%s]", opt->opt_argtyp + 1);
			break;
		}
		n += _t_print_spaces(56 - n);
		n += printf(" %s\n", opt->opt_explanation);
	}
}

static void _t_help()
{
	printf("usage: ./a.out [command [GLOBAL-OPTIONS]]\n");

	printf("commands:\n");
	printf("  teach   to teach neural network\n");
	printf("  synth   to synthesis color image\n");

	printf("options:\n");
	_t_print_options(_G_global_options);
}

int
parseopt(
	Options *anOpts,
	int anArgc,
	char *anArgv[]
	)
{
	int argi, retval;
	char *arg, *val;
	Option const *opt;
	int longopt, needHelp;
	char const *optname;
	char optnamebuf[2] = {0};
	unsigned nerrs;

	needHelp = 0;
	nerrs = 0;
    retval = -2;
    if (!strcmp(anArgv[1], "teach")) {
        retval = 0;
    }
    else if (!strcmp(anArgv[1], "synth")) {
        retval = 1;
    }
    else if (!strcmp(anArgv[1], "--help") || !strcmp(anArgv[1], "-h")) {
        retval = -1;
    }
    else {
        return (retval);
    }

	for (argi = 2; argi < anArgc; ++argi) {
		arg = anArgv[argi];

		if ('-' != arg[0]) {
            printf("warning: %s is not an option\n", arg);
			continue;
		}

		longopt = ('-' == arg[1]);
		++arg;
		if (longopt)
			++arg;

		val = NULL;
		if (longopt && NULL != (val = strchr(arg, '='))) {
			*val = '\0';
			++val;
		}

		if ((!longopt && !strcmp("h", arg)) || (longopt && !strcmp("help", arg))) {
			needHelp = true;
			continue;
		}

		do {
			optname = arg;
			if (!longopt) {
				optnamebuf[0] = *arg;
				optname = optnamebuf;
			}

			opt = (longopt ? _t_long_lookup(arg, _G_global_options)
		                   : _t_short_lookup(*arg, _G_global_options));
			if (NULL == opt) {
				printf("warning: unrecognized option %s", optname);
				continue;
			}

			switch (opt->opt_argtyp[0]) {
			case 'N':
				if (NULL != val) {
					printf("warning: option `%s' doesn't allow an argument", optname);
					val = NULL;
				}
				break;

			case 'R':
				if (!longopt && '\0' != arg[1])
					val = arg + 1;
				if (NULL == val && NULL == (val = anArgv[++argi])) {
					printf("error: option %s requires an argument %s", optname, opt->opt_argtyp + 1);
					++nerrs;
					continue;
				}
				break;

			case 'O':
				if (!longopt && '\0' != arg[1])
					val = arg + 1;
				break;
			}

			if (!opt->opt_parser(anOpts, val)) {
				printf("error: invalid argument for %s", optname);
				++nerrs;
				continue;
			}

		} while (!longopt && NULL == val && '\0' != *++arg);
	}

	if (needHelp) {
		_t_help();
        return (-1);
    }
	else if (0 != nerrs) {
		fprintf(stderr, "try --help\n");
		return (-3);
	}

    return (retval);
}

int opts_default(Options *anOpts)
{
    if (NULL == anOpts->opts_src1 || NULL == anOpts->opts_src2
        || NULL == anOpts->opts_src3 || NULL == anOpts->opts_dst
        || NULL == anOpts->opts_widths || -1 == anOpts->opts_wlen1
        || -1 == anOpts->opts_wlen2 || -1 == anOpts->opts_wlen3) {
        return (1);
    }
    return (0);
}

Options *opts_init(Options *anOpts)
{
    anOpts->opts_src1 = NULL;
    anOpts->opts_src2 = NULL;
    anOpts->opts_src3 = NULL;
    anOpts->opts_dst = NULL;
    anOpts->opts_objmap = NULL;
    anOpts->opts_widths = NULL;
    anOpts->opts_wlen1 = -1;
    anOpts->opts_wlen2 = -1;
    anOpts->opts_wlen3 = -1;

    return (anOpts);
}

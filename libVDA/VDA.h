#ifndef __VDA_VDA_H
# define __VDA_VDA_H

# include "ocv.h"

# define VDA_IR_INITIAL 700

typedef unsigned char VDAElemtype;

extern "C" int VDASynthesis(CvMat*, const CvMat*, const CvMat*, const CvMat*, unsigned, unsigned, unsigned);
extern "C" int VDATeachByImage(const CvMat*, const CvMat*, const CvMat*, const CvMat*, unsigned, unsigned, unsigned);

#endif /* __VDA_VDA_H */

#include "neural.h"

int VDASynthesis(
    CvMat       *aDst,
    const CvMat *aSrc1,
    const CvMat *aSrc2,
    const CvMat *aSrc3,
    unsigned     aWaveLength1,
    unsigned     aWaveLength2,
    unsigned     aWaveLength3
)
{
    VDAElemtype *baseDst, *baseSrc1, *baseSrc2, *baseSrc3;
    unsigned wl1, wl2, wl3;
    int width, height, i;

    if (NULL == aDst || NULL == aSrc1 || NULL == aSrc2 || NULL == aSrc3) {
        return (-1);
    }

    width = aDst->cols;
    height = aDst->rows;
    if (width != aSrc1->cols || width != aSrc2->cols || width != aSrc3->cols) {
        return (-1);
    }
    if (height != aSrc1->rows || height != aSrc2->rows || height != aSrc3->rows) {
        return (-1);
    }

    baseDst = aDst->data.ptr;
    baseSrc1 = aSrc1->data.ptr;
    baseSrc2 = aSrc2->data.ptr;
    baseSrc3 = aSrc3->data.ptr;

    wl1 = aWaveLength1 - VDA_IR_INITIAL;
    wl2 = aWaveLength2 - VDA_IR_INITIAL;
    wl3 = aWaveLength3 - VDA_IR_INITIAL;
    if (0 != VDAReadWidths("width_in.txt")) {
        return (-1);
    }
#if 0
    for (i = 0; i < VDA_NEURAL_WIDHT_COUNT; i++)
        printf("%lf\n", VDA_WIDTHS[i]);
#endif

    //for (i = (width * height - 1); i < width * height; i++) {
    for (i = 0; i < width * height; i++) {
        VDANeuralOut res;
        //printf("%u %u %u\n", baseSrc1[i] * wl1, baseSrc2[i] * wl2, baseSrc3[i] * wl3);
        res = VDAMakeRGB(baseSrc1[i], wl1, baseSrc2[i], wl2, baseSrc3[i], wl3);
        //printf("%u %u %u\n", baseDst[i * 3 + 0], baseDst[i * 3 + 1], baseDst[i * 3 + 2]);
        //printf("%.3lf %.3lf %.3lf\n", res.neu_val[0], res.neu_val[1], res.neu_val[2]);
#if 0
        baseDst[i * 3 + 0] = res.neu_val[0];
        baseDst[i * 3 + 1] = res.neu_val[1];
        baseDst[i * 3 + 2] = res.neu_val[2];
#else
        baseDst[i * 3 + 0] = baseSrc1[i];
        baseDst[i * 3 + 1] = baseSrc2[i];
        baseDst[i * 3 + 2] = baseSrc3[i];
#endif
        //printf("%u %u %u\n", baseDst[i * 3 + 0], baseDst[i * 3 + 1], baseDst[i * 3 + 2]);
    }

    return (0);
}

int VDATeachByImage(
    const CvMat *aDst,
    const CvMat *aSrc1,
    const CvMat *aSrc2,
    const CvMat *aSrc3,
    unsigned     aWaveLength1,
    unsigned     aWaveLength2,
    unsigned     aWaveLength3
)
{
    int width, height;

    if (NULL == aDst || NULL == aSrc1 || NULL == aSrc2 || NULL == aSrc3) {
        return (-1);
    }

    width = aDst->cols;
    height = aDst->rows;
    if (width != aSrc1->cols || width != aSrc2->cols || width != aSrc3->cols) {
        return (-1);
    }
    if (height != aSrc1->rows || height != aSrc2->rows || height != aSrc3->rows) {
        return (-1);
    }

    VDATeachByArray(aDst->data.ptr, aSrc1->data.ptr, aSrc2->data.ptr, aSrc3->data.ptr, aWaveLength1, aWaveLength2, aWaveLength3, width * height);

    return (0);
}

#ifndef __VDA_NEURAL_H
# define __VDA_NEURAL_H

# include "VDA.h"
#if 0 /* 3 rows best result: 2.538 */
# define VDA_NEURAL_EPSILON       0.0005
# define VDA_NEURAL_ALPHA         0.005
# define VDA_NEURAL_EPOCH_NUM     650
#elif 1
# define VDA_NEURAL_EPSILON       0.0005
# define VDA_NEURAL_ALPHA         0.008
# define VDA_NEURAL_EPOCH_NUM     1000000
#elif 0 /* 4 rows best result: 2.978 */
# define VDA_NEURAL_EPSILON       0.0003
# define VDA_NEURAL_ALPHA         0.005
# define VDA_NEURAL_EPOCH_NUM     223
//# define VDA_NEURAL_EPOCH_NUM     1000
#elif 0 /* 4 rows best?? result: 2.624 */
# define VDA_NEURAL_EPSILON       0.005
# define VDA_NEURAL_ALPHA         0.05
# define VDA_NEURAL_EPOCH_NUM     29
#elif 0 /* test */
# define VDA_NEURAL_EPSILON       0.00007
# define VDA_NEURAL_ALPHA         0.05
# define VDA_NEURAL_EPOCH_NUM     1000
#else /* test */
# define VDA_NEURAL_EPSILON       0.00005
# define VDA_NEURAL_ALPHA         0.005
# define VDA_NEURAL_EPOCH_NUM     1000
#endif

#if 1
# define VDA_NEURAL_INPUT_NUM     3
# define VDA_NEURAL_HIDDEN_NUM    100
# define VDA_NEURAL_OUTPUT_NUM    3
//# define VDA_NEURAL_WIDHT_COUNT   21 /* (VDA_NEURAL_INPUT_NUM + 1) * VDA_NEURAL_OUTPUT_NUM */
/* 4 * 6 + 7 * 3 = 24 + 21 = 45 */
# if 1
#  define VDA_NEURAL_STEP_1        ((VDA_NEURAL_INPUT_NUM + 1) * VDA_NEURAL_HIDDEN_NUM)
#  define VDA_NEURAL_STEP_2        ((VDA_NEURAL_HIDDEN_NUM + 1) * VDA_NEURAL_OUTPUT_NUM)
#  define VDA_NEURAL_WIDHT_COUNT   (VDA_NEURAL_STEP_1 + VDA_NEURAL_STEP_2)
# else
#  define VDA_NEURAL_STEP_1        24 /* (VDA_NEURAL_INPUT_NUM + 1) * VDA_NEURAL_HIDDEN_COUNT */
#  define VDA_NEURAL_STEP_2        21 /* (VDA_NEURAL_HIDDEN_COUNT + 1) * VDA_NEURAL_OUTPUT_NUM */
#  define VDA_NEURAL_WIDHT_COUNT   45 /* (VDA_NEURAL_INPUT_NUM + 1) * VDA_NEURAL_HIDDEN_COUNT + (VDA_NEURAL_HIDDEN_COUNT + 1) * VDA_NEURAL_OUTPUT_NUM */
#endif
#else /* test */
# define VDA_NEURAL_INPUT_NUM     6
# define VDA_NEURAL_HIDDEN_NUM    4
# define VDA_NEURAL_OUTPUT_NUM    3
//# define VDA_NEURAL_WIDHT_COUNT   21 /* (VDA_NEURAL_INPUT_NUM + 1) * VDA_NEURAL_OUTPUT_NUM */
/* 7 * 7 + 8 * 3 = 49 + 24 = 73 */
# define VDA_NEURAL_STEP_1        49 /* (VDA_NEURAL_INPUT_NUM + 1) * VDA_NEURAL_HIDDEN_COUNT */
# define VDA_NEURAL_STEP_2        24 /* (VDA_NEURAL_HIDDEN_COUNT + 1) * VDA_NEURAL_OUTPUT_NUM */
# define VDA_NEURAL_WIDHT_COUNT   73 /* (VDA_NEURAL_INPUT_NUM + 1) * VDA_NEURAL_HIDDEN_COUNT + (VDA_NEURAL_HIDDEN_COUNT + 1) * VDA_NEURAL_OUTPUT_NUM */
#endif

struct __vda_neural_in {
    double neu_val[VDA_NEURAL_INPUT_NUM];
};
struct __vda_neural_hidden {
    double neu_val[VDA_NEURAL_HIDDEN_NUM];
};
struct __vda_neural_out {
    double neu_val[VDA_NEURAL_OUTPUT_NUM];
};

typedef struct __vda_neural_in VDANeuralIn;
typedef struct __vda_neural_hidden VDANeuralHidden;
typedef struct __vda_neural_out VDANeuralOut;

extern double VDA_WIDTHS[VDA_NEURAL_WIDHT_COUNT];

extern int          VDATeachByArray(VDAElemtype *, const VDAElemtype *, const VDAElemtype *, const VDAElemtype *, unsigned, unsigned, unsigned, unsigned);
extern VDANeuralOut VDAMakeRGB(unsigned, unsigned, unsigned, unsigned, unsigned, unsigned);
extern int          VDAReadWidths(const char *);
extern int          VDAWriteWidths(const char *, double[VDA_NEURAL_WIDHT_COUNT]);

#endif /* __VDA_NEURAL_H */

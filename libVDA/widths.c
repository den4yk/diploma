#include "neural.h"
#include <stdio.h>

double VDA_WIDTHS[VDA_NEURAL_WIDHT_COUNT];

int VDAReadWidths(const char *aFileName)
{
    unsigned i;
    FILE *f = fopen(aFileName, "r");

    if (NULL == f) {
        return (-1);
    }

    for (i = 0; i < VDA_NEURAL_WIDHT_COUNT; i++) {
        if (EOF == fscanf(f, "%lf", &VDA_WIDTHS[i]))
            break;
        //printf("%lf\n", VDA_WIDTHS[i]);
    }

    fclose(f);

    if (i != VDA_NEURAL_WIDHT_COUNT) {
        return (-2);
    }
    return (0);
}

int VDAWriteWidths(const char *aFileName, double aWidths[VDA_NEURAL_WIDHT_COUNT])
{
    unsigned i;
    FILE *f = fopen(aFileName, "w");

    for (i = 0; i < VDA_NEURAL_WIDHT_COUNT; i++) {
        fprintf(f, "%.100lf\n", aWidths[i]);
    }

    fclose(f);
    return (0);
}

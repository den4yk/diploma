#include "neural.h"

#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

int kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if(ch != EOF)
        {
            ungetc(ch, stdin);
            return 1;
              
        }

    return 0;
    
}

static double __VDAExp(double x)
{
    union { double d; long long i; } u;
    u.i = (long long)(6497320848556798LL * x + 0x3fef127e83d16f12LL);
    return (u.d);
}

static double __VDASigmoid(double x)
{
    return (1.0 / (1 + __VDAExp(-x)));
}

static double __VDASigmoidDerivative(double x)
{
#if 0
    double expX = __VDAExp(-x) + 1;
    return ((expX - 1) / (expX * expX));
#else
    double sigm = __VDASigmoid(x);
    return (sigm * (1 - sigm));
#endif
}

static VDANeuralOut __VDATeachOut(
    VDANeuralOut *anOutput,
    VDANeuralOut  anInput,
    double        aWidths[VDA_NEURAL_WIDHT_COUNT],
    double       *anErr
)
{
    VDANeuralOut output, retval;
    unsigned i;

    for (i = 0; i < VDA_NEURAL_OUTPUT_NUM; i++) {
        output.neu_val[i] = __VDASigmoid(anInput.neu_val[i]);
        //(*anErr) += ((output.neu_val[i] - anOutput->neu_val[i]) * (output.neu_val[i] - anOutput->neu_val[i]));
        (*anErr) += ((255 * output.neu_val[i] - 255 * anOutput->neu_val[i]) * (255 * output.neu_val[i] - 255 * anOutput->neu_val[i]));
        retval.neu_val[i] = (anOutput->neu_val[i] - output.neu_val[i]) * __VDASigmoidDerivative(output.neu_val[i]);
    }

    //printf("err: %.3lf%% %.3lf %.3lf %.3lf %.3lf %.3lf %.3lf\n", err * 100, 255.0 * output.neu_val[0], 255.0 * output.neu_val[1], 255.0 * output.neu_val[2], 255.0 * anOutput->neu_val[0], 255.0 * anOutput->neu_val[1], 255.0 * anOutput->neu_val[2]);

    anOutput->neu_val[0] = output.neu_val[0];
    anOutput->neu_val[1] = output.neu_val[1];
    anOutput->neu_val[2] = output.neu_val[2];

    return (retval);
}

static VDANeuralHidden __VDATeachHidden(
    VDANeuralOut    *anOutput,
    VDANeuralHidden  anInput,
    double           aWidths[VDA_NEURAL_WIDHT_COUNT],
    double          *anErr
)
{
    VDANeuralHidden output, retval;
    VDANeuralOut input, deltas;
    unsigned i, j;
    static double dwidths[VDA_NEURAL_STEP_2] = {0};

    for (i = 0; i < VDA_NEURAL_HIDDEN_NUM; i++)
        output.neu_val[i] = __VDASigmoid(anInput.neu_val[i]);

    for (i = 0; i < VDA_NEURAL_OUTPUT_NUM; i++) {
        input.neu_val[i] = aWidths[VDA_NEURAL_STEP_1 + (VDA_NEURAL_HIDDEN_NUM * VDA_NEURAL_OUTPUT_NUM) + i];
        for (j = 0; j < VDA_NEURAL_HIDDEN_NUM; j++)
            input.neu_val[i] += output.neu_val[j] * aWidths[VDA_NEURAL_STEP_1 + j * VDA_NEURAL_OUTPUT_NUM + i];
    }

    deltas = __VDATeachOut(anOutput, input, aWidths, anErr);

    for (i = 0; i < VDA_NEURAL_HIDDEN_NUM; i++) {
        double x = 0;
        for (j = 0; j < VDA_NEURAL_OUTPUT_NUM; j++)
            x += deltas.neu_val[j] * aWidths[VDA_NEURAL_STEP_1 + i * VDA_NEURAL_OUTPUT_NUM + j];
        retval.neu_val[i] = __VDASigmoidDerivative(output.neu_val[i]) * x;
    }

    for (i = 0; i < VDA_NEURAL_STEP_2 - VDA_NEURAL_OUTPUT_NUM; i++) {
        dwidths[i] = VDA_NEURAL_EPSILON * output.neu_val[i / VDA_NEURAL_OUTPUT_NUM] * deltas.neu_val[i % VDA_NEURAL_OUTPUT_NUM] + VDA_NEURAL_ALPHA * dwidths[i];
        aWidths[VDA_NEURAL_STEP_1 + i] += dwidths[i];
    }

    for (i = (VDA_NEURAL_HIDDEN_NUM * VDA_NEURAL_OUTPUT_NUM); i < VDA_NEURAL_STEP_2; i++) {
        dwidths[i] = VDA_NEURAL_EPSILON * deltas.neu_val[i % VDA_NEURAL_OUTPUT_NUM] + VDA_NEURAL_ALPHA * dwidths[i];
        aWidths[VDA_NEURAL_STEP_1 + i] += dwidths[i];
    }

    return (retval);
}

static int __VDATeach(
    VDANeuralOut *anOutput,
    unsigned      anElem1,
    unsigned      aWidth1,
    unsigned      anElem2,
    unsigned      aWidth2,
    unsigned      anElem3,
    unsigned      aWidth3,
    double        aWidths[VDA_NEURAL_WIDHT_COUNT],
    double       *anErr
)
{
    VDANeuralIn neuralIn;
    VDANeuralHidden input, deltas;
    unsigned i, j;
    static double dwidths[VDA_NEURAL_STEP_1] = {0};
#if 0
    neuralIn.neu_val[0] = (double)anElem1 / 255.0;
    neuralIn.neu_val[1] = (0 != aWidth1 ? 1.0 / (double)aWidth1 : 1);
    neuralIn.neu_val[2] = (double)anElem2 / 255.0;
    neuralIn.neu_val[3] = (0 != aWidth2 ? 1.0 / (double)aWidth2 : 1);
    neuralIn.neu_val[4] = (double)anElem3 / 255.0;
    neuralIn.neu_val[5] = (0 != aWidth3 ? 1.0 / (double)aWidth3 : 1);
#else
    neuralIn.neu_val[0] = (double)anElem1 / 255.0;
    neuralIn.neu_val[1] = (double)anElem2 / 255.0;
    neuralIn.neu_val[2] = (double)anElem3 / 255.0;
#endif

    for (i = 0; i < VDA_NEURAL_HIDDEN_NUM; i++) {
        input.neu_val[i] = aWidths[(VDA_NEURAL_INPUT_NUM * VDA_NEURAL_HIDDEN_NUM) + i];
        for (j = 0; j < VDA_NEURAL_INPUT_NUM; j++)
            input.neu_val[i] += neuralIn.neu_val[j] * aWidths[j * VDA_NEURAL_HIDDEN_NUM + i];
    }

    deltas = __VDATeachHidden(anOutput, input, aWidths, anErr);
    //deltas = __VDATeachOut(anOutput, input, aWidths, anErr);

    for (i = 0; i < VDA_NEURAL_STEP_1 - VDA_NEURAL_HIDDEN_NUM; i++) {
        dwidths[i] = VDA_NEURAL_EPSILON * neuralIn.neu_val[i / VDA_NEURAL_HIDDEN_NUM] * deltas.neu_val[i % VDA_NEURAL_HIDDEN_NUM] + VDA_NEURAL_ALPHA * dwidths[i];
        aWidths[i] += dwidths[i];
    }

    for (i = (VDA_NEURAL_INPUT_NUM * VDA_NEURAL_HIDDEN_NUM); i < VDA_NEURAL_STEP_1; i++) {
        dwidths[i] = VDA_NEURAL_EPSILON * deltas.neu_val[i % VDA_NEURAL_HIDDEN_NUM] + VDA_NEURAL_ALPHA * dwidths[i];
        aWidths[i] += dwidths[i];
    }

    return (0);
}

int VDATeachByArray(
    VDAElemtype       *aResValues,
    const VDAElemtype *anInputValues1,
    const VDAElemtype *anInputValues2,
    const VDAElemtype *anInputValues3,
    unsigned           aWaveLength1,
    unsigned           aWaveLength2,
    unsigned           aWaveLength3,
    unsigned           anArraySize
)
{
    unsigned epoch, i, wl1, wl2, wl3, min_epoch;
    double widths[VDA_NEURAL_WIDHT_COUNT], min_err = -1;

    wl1 = wl2 = wl3 = 1;
#if 1
    for (i = 0; i < VDA_NEURAL_WIDHT_COUNT; i++) {
        widths[i] = 1.0 / (double)(rand() % 100 + 1);
    //    widths[i] = VDA_WIDTHS[i];
    }
#else
    VDAReadWidths("width_in.txt");
    for (i = 0; i < VDA_NEURAL_WIDHT_COUNT; i++) {
        widths[i] = VDA_WIDTHS[i];
    }
#endif
    anArraySize -= 110*800;
#if 0
    for (i = 0; i < VDA_NEURAL_WIDHT_COUNT; i++)
        printf("%lf\n", widths[i]);
    wl1 = aWaveLength1 - VDA_IR_INITIAL;
    wl2 = aWaveLength2 - VDA_IR_INITIAL;
    wl3 = aWaveLength3 - VDA_IR_INITIAL;
#endif

    for (epoch = 0; epoch < VDA_NEURAL_EPOCH_NUM; epoch++) {
        double err = 0;
        //for (i = anArraySize - 1; i < anArraySize; i++) {
        //for (i = 0; i < anArraySize; i += 50) {
        for (i = 0; i < anArraySize; i += 50) {
        //for (i = 0; i < 100; i++) {
        //for (i = 30*800 + 25; i < (anArraySize - 130*800 - 40); i++) {
        //for (i = 0; i < 280100; i++) {
        //for (i = 791000; i < 3500000; i++) {
            if (0 != ((i / 800) % 50)) {
                i += 800 * 49;
                continue;
            }
            VDANeuralOut out = {{
                ((double)aResValues[3 * i + 0] / 255.0),
                ((double)aResValues[3 * i + 1] / 255.0),
                ((double)aResValues[3 * i + 2] / 255.0)
            }};
            __VDATeach(&out, anInputValues1[i], wl1, anInputValues2[i], wl2, anInputValues3[i], wl3, widths, &err);
#if 0
            if (0 && i == 32050) {
                printf("%.3lf %.3lf %.3lf %u %u %u\n", 255*out.neu_val[0], 255*out.neu_val[1], 255*out.neu_val[2], aResValues[3*i+0], aResValues[3*i+1], aResValues[3*i+2]);
            }
            if (0 == i) {
                printf("1:%.3lf %.3lf %.3lf %u %u %u\n", 255*out.neu_val[0], 255*out.neu_val[1], 255*out.neu_val[2], aResValues[3*i+0], aResValues[3*i+1], aResValues[3*i+2]);
                __VDATeach(&out, anInputValues1[i], wl1, anInputValues2[i], wl2, anInputValues3[i], wl3, widths, &err);
                printf("2:%.3lf %.3lf %.3lf %u %u %u\n", 255*out.neu_val[0], 255*out.neu_val[1], 255*out.neu_val[2], aResValues[3*i+0], aResValues[3*i+1], aResValues[3*i+2]);
            }
#endif
            if (epoch == (VDA_NEURAL_EPOCH_NUM - 1)) {
                printf("%u %u %u\n", aResValues[3*i+0], aResValues[3*i+1], aResValues[3*i+2]);
                aResValues[3 * i + 0] = 255.0 * out.neu_val[0];
                aResValues[3 * i + 1] = 255.0 * out.neu_val[1];
                aResValues[3 * i + 2] = 255.0 * out.neu_val[2];
                printf("%u %u %u\n", aResValues[3*i+0], aResValues[3*i+1], aResValues[3*i+2]);
            }
        }
        err /= (anArraySize / 2500) * 3;
        //err /= 300;
        printf("epoch: %u; err: %.3lf%%\n", epoch, (sqrt(err) / 255.0) * 100);
        if (err < 1 || kbhit())
            break;
        if (min_err < 0 || err < min_err) {
            min_err = err;
            min_epoch = epoch;
        }
        printf("min_epoch: %u; min_err: %.3lf%%\n", min_epoch, (sqrt(err) / 255.0) * 100);
    }

    VDAWriteWidths("width_out.txt", widths);
#if 0
    for (i = 0; i < VDA_NEURAL_WIDHT_COUNT; i++)
        printf("%.100lf\n", widths[i]);
#endif

    return (0);
}

static VDANeuralOut __VDAMakeRGBOut(
    VDANeuralOut anInput
)
{
    VDANeuralOut output;
    unsigned i;

    for (i = 0; i < VDA_NEURAL_OUTPUT_NUM; i++)
        output.neu_val[i] = __VDASigmoid(anInput.neu_val[i]);

    return (output);
}

static VDANeuralOut __VDAMakeRGBHidden(
    VDANeuralHidden anInput
)
{
    VDANeuralHidden output;
    VDANeuralOut input;
    unsigned i, j;

    for (i = 0; i < VDA_NEURAL_HIDDEN_NUM; i++)
        output.neu_val[i] = __VDASigmoid(anInput.neu_val[i]);

    for (i = 0; i < VDA_NEURAL_OUTPUT_NUM; i++) {
        input.neu_val[i] = VDA_WIDTHS[VDA_NEURAL_STEP_1 + (VDA_NEURAL_HIDDEN_NUM * VDA_NEURAL_OUTPUT_NUM) + i];
        for (j = 0; j < VDA_NEURAL_HIDDEN_NUM; j++)
            input.neu_val[i] += output.neu_val[j] * VDA_WIDTHS[VDA_NEURAL_STEP_1 + j * VDA_NEURAL_OUTPUT_NUM + i];
    }

    return (__VDAMakeRGBOut(input));
}

VDANeuralOut VDAMakeRGB(
    unsigned anElem1,
    unsigned aWidth1,
    unsigned anElem2,
    unsigned aWidth2,
    unsigned anElem3,
    unsigned aWidth3
)
{
    VDANeuralIn neuralIn;
    VDANeuralHidden input;
    VDANeuralOut output;
    unsigned i, j;
#if 0
    neuralIn.neu_val[0] = (double)anElem1 / 255.0;
    neuralIn.neu_val[1] = (0 != aWidth1 ? 1.0 / (double)aWidth1 : 1);
    neuralIn.neu_val[2] = (double)anElem2 / 255.0;
    neuralIn.neu_val[3] = (0 != aWidth2 ? 1.0 / (double)aWidth2 : 1);
    neuralIn.neu_val[4] = (double)anElem3 / 255.0;
    neuralIn.neu_val[5] = (0 != aWidth3 ? 1.0 / (double)aWidth3 : 1);
#else
    neuralIn.neu_val[0] = (double)anElem1 / 255.0;
    neuralIn.neu_val[1] = (double)anElem2 / 255.0;
    neuralIn.neu_val[2] = (double)anElem3 / 255.0;
#endif

    for (i = 0; i < VDA_NEURAL_HIDDEN_NUM; i++) {
        input.neu_val[i] = VDA_WIDTHS[(VDA_NEURAL_INPUT_NUM * VDA_NEURAL_HIDDEN_NUM) + i];
        for (j = 0; j < VDA_NEURAL_INPUT_NUM; j++)
            input.neu_val[i] += neuralIn.neu_val[j] * VDA_WIDTHS[j * VDA_NEURAL_HIDDEN_NUM + i];
    }

    output = __VDAMakeRGBHidden(input);
    //output = __VDAMakeRGBOut(input);

    for (i = 0; i < VDA_NEURAL_OUTPUT_NUM; i++)
        output.neu_val[i] = 255.0 * output.neu_val[i];

    return (output);
}

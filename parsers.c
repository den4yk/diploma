#include "parsers.h"

static int _t_spacesP(char const *aStr, char const **anEndPtr)
{
	for (; isspace(*aStr); ++aStr);
	*anEndPtr = aStr;
	return (true);
}

static int _t_chrP(int aChr, char const *aStr, char const **anEndPtr)
{
	*anEndPtr = (aChr == *aStr ? aStr + 1 : aStr);
	return (aChr == *aStr);
}

int eofP(char const *aStr)
{
	return (_t_spacesP(aStr, &aStr) && '\0' == *aStr);
}

int intP(int *aResPtr, char const *aStr, char const **anEndPtr)
{
	int parsed;
	int x;
	char *end;

	x = strtol(aStr, &end, 0);
	/* the function `strtol' skips leading white spaces,
	 * that's why we check !isspace(end[-1]) */
	parsed = (aStr != end && !isspace(end[-1]));
	if (parsed)
		*aResPtr = x;
	if (NULL != anEndPtr)
		*anEndPtr = end;
	return (parsed);
}

int
int3P(
	int *aResPtr1,
	int *aResPtr2,
	int *aResPtr3,
	char const *aStr,
	char const **anEndPtr
	)
{
	int parsed;
	int x, y, z;

	parsed = (_t_chrP('(', aStr, anEndPtr) && _t_spacesP(*anEndPtr, anEndPtr)
		&& intP(&x, *anEndPtr, anEndPtr) && _t_spacesP(*anEndPtr, anEndPtr)
		&& _t_chrP(',', *anEndPtr, anEndPtr) && _t_spacesP(*anEndPtr, anEndPtr)
		&& intP(&y, *anEndPtr, anEndPtr) && _t_spacesP(*anEndPtr, anEndPtr)
		&& _t_chrP(',', *anEndPtr, anEndPtr) && _t_spacesP(*anEndPtr, anEndPtr)
		&& intP(&z, *anEndPtr, anEndPtr) && _t_spacesP(*anEndPtr, anEndPtr)
		&& _t_chrP(')', *anEndPtr, anEndPtr)
	);
	
	if (parsed) {
		*aResPtr1 = x;
		*aResPtr2 = y;
		*aResPtr3 = z;
    }

	return (parsed);
}

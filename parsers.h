#ifndef __PARSERS__
# define __PARSERS__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct __options {
    char const *opts_src1, *opts_src2, *opts_src3;
    char const *opts_dst, *opts_widths, *opts_objmap;
    int         opts_wlen1, opts_wlen2, opts_wlen3;
};

typedef struct __options Options;

typedef int   (*opt_parser_fn)(Options *, char *);

struct __option {
	char const   *opt_short, *opt_long;
	opt_parser_fn opt_parser;
	char const   *opt_argtyp;
	char const   *opt_explanation;
};

typedef struct __option Option;

extern Option const _G_global_options[];

extern int      opts_default(Options *);
extern Options *opts_init(Options *);

extern int parseopt(Options *, int, char **); 

extern int eofP(char const *);
extern int intP(void *, char const *, char const **);
extern int int3P(int *, int *, int *, char const *, char const **);

#endif /* !defined(__PARSERS__) */

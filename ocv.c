#include "ocv.h"

int ocv_readImage(CvMat **aMat, const char *aFileName, int isRGB) {
    *aMat = cvLoadImageM(aFileName, (isRGB ? CV_LOAD_IMAGE_COLOR : CV_LOAD_IMAGE_GRAYSCALE));
    return (NULL != (*aMat) ? 0 : -1);
}

int ocv_writeImage(const CvMat *aMat, const char *aFileName) {
    return (1 == cvSaveImage(aFileName, aMat, 0) ? 0 : -1);
}

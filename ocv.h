#ifndef __OCV_H
# define __OCV_H
//# include <opencv/cv.h>
//# include "opencv2/imgproc/imgproc_c.h"
# include "opencv2/imgcodecs/imgcodecs_c.h"

extern int ocv_readImage(CvMat**, const char*, int);
extern int ocv_writeImage(const CvMat*, const char*);

#endif /* __OCV_H */
